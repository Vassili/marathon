package fr.imie.Marathon.dal;

import fr.imie.Marathon.bo.Coureur;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface DaoMarathon extends CrudRepository<Coureur,Integer> {

//    void updateByHdepart(Date date);

    Coureur findCoureurByNumDossard(String dossard);

    List<Coureur>  findByOrderByIdDesc();
}
