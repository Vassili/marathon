package fr.imie.Marathon.bo;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Coureur {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String nom;
    private Genre sexe;
    private Date hDepart;
    private Date hArrive;
    private String numDossard;

    public Coureur() {
    }

    public Coureur(String nom, Genre sexe, Date hDepart, Date hArrive) {
        this.nom = nom;
        this.sexe = sexe;
        this.hDepart = hDepart;
        this.hArrive = hArrive;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Genre getSexe() {
        return sexe;
    }

    public void setSexe(Genre sexe) {
        this.sexe = sexe;
    }

    public Date gethDepart() {
        return hDepart;
    }

    public void sethDepart(Date hDepart) {
        this.hDepart = hDepart;
    }

    public Date gethArrive() {
        return hArrive;
    }

    public void sethArrive(Date hArrive) {
        this.hArrive = hArrive;
    }

    public String getNumDossard() {
        return numDossard;
    }

    public void setNumDossard(String numDossard) {
        this.numDossard = numDossard;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Coureur{");
        sb.append("id=").append(id);
        sb.append(", nom='").append(nom).append('\'');
        sb.append(", sexe=").append(sexe);
        sb.append(", hDepart=").append(hDepart);
        sb.append(", hArrive=").append(hArrive);
        sb.append(", numDossard=").append(numDossard);
        sb.append('}');
        return sb.toString();
    }
}
