package fr.imie.Marathon.bll;

import fr.imie.Marathon.bo.Coureur;
import fr.imie.Marathon.dal.DaoMarathon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class ManagerMarathonImpl implements Manager {

    @Autowired
    DaoMarathon dao;

    @Override
    public void add(Coureur coureur) {
        dao.save(coureur);
    }

    @Override
    public void start() {
//       dao.updateByHdepart(new Date());
    }

    @Override
    public void arrive(String dossard) {
        Coureur c = dao.findCoureurByNumDossard(dossard);

        c.sethArrive(new Date());

        dao.save(c);
    }

    @Override
    public List<Coureur> getClassement() {
        return dao.findByOrderByIdDesc();
    }
}
