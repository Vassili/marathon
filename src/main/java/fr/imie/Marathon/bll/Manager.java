package fr.imie.Marathon.bll;

import fr.imie.Marathon.bo.Coureur;

import java.util.List;

public interface Manager {

    public void add(Coureur coureur);

    public void start();

    public void arrive(String dossard);

    public List<Coureur> getClassement();
}
