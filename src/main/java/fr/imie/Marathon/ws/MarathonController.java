package fr.imie.Marathon.ws;

import fr.imie.Marathon.bll.Manager;
import fr.imie.Marathon.bo.Coureur;
import fr.imie.Marathon.bo.Genre;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/Marathon")
public class MarathonController {

    @Autowired
    Manager marathon;

    @GetMapping("/start")
    public void start(){
        marathon.start();
    }

    @GetMapping("/Arrive")
    public void arrive(@RequestParam String dossard){
        marathon.arrive(dossard);
    }

    @GetMapping("/Classement")
    public List<Coureur> classement(){
        return marathon.getClassement();
    }

    @PostMapping("/AjoutCoureur")
    public void ajoutCoureur(@RequestParam String nom, @RequestParam Genre genre, @RequestParam String numDossard){
        Coureur c = new Coureur();

        c.setNom(nom);
        c.setSexe(genre);
        c.setNumDossard(numDossard);

        marathon.add(c);
    }
}
